<?php

function createSyllable() {
	$vowels = [ 'a', 'e', 'i', 'o', 'u', 'y' ];
	$consonants = [ 'b', 'c', 'ch', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 'sh', 't', 'th', 'v', 'w', 'x', 'y', 'z', 'zh' ];
	
	$length = mt_rand( 1, 5 );

	if ( $length > 2 ) {
		$length = 1;
	}

	$vowelSet = '';

	for ( $i = 0; $i < $length; $i++ ) {
		$vowelSet .= randomElement( $vowels );
	}

	$firstConsonant = randomElement( $consonants );
	$endingConsonant = randomElement( $consonants );

	$syllable = $firstConsonant . $vowelSet;

	if ( mt_rand( 0, 3 ) > 2 ) {
		$syllable .= $endingConsonant;
	}

	return $syllable;
}

function createWord( Array $syllables ) {
	$word = '';

	for ( $i = 0; $i < count( $syllables ); $i++ ) {
		if ( $i < count( $syllables ) && $i > 0 ) {
			if ( mt_rand( 0, 10 ) < 2 ) {
				$word .= "'";
			}
		}
		$word .= $syllables[ $i ];
	}

	$word = ucfirst( $word );

	return $word;
}

function dd( $variable ) {
	die( var_dump( $variable ) );
}

function randomElement( Array $array ) {
	return $array[ mt_rand( 0, count( $array ) - 1 ) ];
}

$numberOfIterations = isset( $argv[ 1 ] ) ? ( int )$argv[ 1 ] : 1;

for ( $i = 0; $i < $numberOfIterations; $i++ ) {
	$syllables = [];

	$numberOfSyllables = mt_rand( 1, 4 );

	for ( $j = 0; $j < $numberOfSyllables; $j++ ) {
		$syllables[] = createSyllable();
	}

	$word = createWord( $syllables );

	echo "$word\n";
}

